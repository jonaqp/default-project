/**
 * Menu block
 * Acts as a sample js file/target for menu logic
 * @module
 */


/**
 * Menu class
 * Target for menu logic
 * @class
 */
class Menu {}


// Export
export default Menu;
