This directory only contains component specific JavaScript tests.
Every test file in this directory should be named <component>.spec.js.

Example: test suite for js/components/sample.js should be test/components/sample.spec.js

Every test suite should import it's own module:

    import { sampleA, sampleB } from 'components/sample';
